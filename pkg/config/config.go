package config

import (
	"github.com/joho/godotenv"
	"github.com/spf13/viper"
	"log"
	"os"
)

type Config struct {
	BaseGameSvcUrl string `mapstructure:"BASE_GAME_SVC_URL"`
	QuizAPIUrl     string `mapstructure:"BASE_QUIZ_API_URL"`
	DebugON        bool   `mapstructure:"DEBUG_ON"`
	DBUrl          string `mapstructure:"DB_URL"`
	DBUrlDocker    string `mapstructure:"DB_URL_DOCKER"`
	Port           string `mapstructure:"PORT"`
	RequestTimeout int    `mapstructure:"REQUEST_TIMEOUT"`
	DockerPath     string
}

func LoadConfig() (*Config, error) {
	var c Config

	e := godotenv.Load(".env") //Загрузить файл .env из корня приложения
	if e != nil {
		log.Println(e)
	}

	dockerPath := os.Getenv("APP_PATH")
	c.DockerPath = dockerPath

	viper.AddConfigPath("./config/envs")     // Файлы окружения для приложения
	viper.AddConfigPath(dockerPath)          // Путь для приложения в docker контейнере
	viper.AddConfigPath("../../config/envs") // Для тестов

	viper.SetConfigName("prod")
	viper.SetConfigType("env")

	viper.AutomaticEnv()

	err := viper.ReadInConfig()

	if err != nil {
		viper.SetConfigName("dev")
		viper.SetConfigType("env")
		viper.AutomaticEnv()

		err = viper.ReadInConfig()

		if err != nil {
			return nil, err
		}
		err = viper.Unmarshal(&c)

		return &c, nil
	}

	err = viper.Unmarshal(&c)

	if err != nil {
		return nil, err
	}

	return &c, nil
}
