package mock_logger

type MockLogger struct {
	DebugFunc func(msg string)
	InfoFunc  func(msg string)
	InfofFunc func(template, msg string)
	WarnFunc  func(msg string)
	ErrorFunc func(msg string)
}

func (m *MockLogger) Debug(msg string) {
	if m.DebugFunc != nil {
		m.DebugFunc(msg)
	}
}

func (m *MockLogger) Info(msg string) {
	if m.InfoFunc != nil {
		m.InfoFunc(msg)
	}
}

func (m *MockLogger) Infof(template, msg string) {
	if m.InfofFunc != nil {
		m.InfofFunc(template, msg)
	}
}

func (m *MockLogger) Warn(msg string) {
	if m.WarnFunc != nil {
		m.WarnFunc(msg)
	}
}

func (m *MockLogger) Error(msg string) {
	if m.ErrorFunc != nil {
		m.ErrorFunc(msg)
	}
}
