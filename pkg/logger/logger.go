package logger

import (
	li "gitlab.com/Russi2020QuizGroup/game-svc/pkg/logger/interfaces"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"log"
	"os"
	"time"
)

var _ li.Logger = (*CustomLogger)(nil)

type CustomLogger struct {
	logger *zap.Logger
}

func NewLogger(debugON bool) (li.Logger, error) {
	infoLevel := zap.LevelEnablerFunc(func(lvl zapcore.Level) bool {
		return lvl <= zapcore.WarnLevel
	})

	errorLevel := zap.LevelEnablerFunc(func(lvl zapcore.Level) bool {
		return lvl >= zapcore.ErrorLevel
	})

	var options []zap.Option
	if debugON {
		options = append(options, zap.AddCaller())
		options = append(options, zap.AddCallerSkip(1)) // 1 for these function itself
	}

	// Create separate cores for info.log and error.log files
	infoCore := zapcore.NewCore(zapcore.NewJSONEncoder(zap.NewProductionEncoderConfig()), zapcore.AddSync(&CustomFileWriter{filepath: "info.log"}), infoLevel)
	errorCore := zapcore.NewCore(zapcore.NewJSONEncoder(zap.NewProductionEncoderConfig()), zapcore.AddSync(&CustomFileWriter{filepath: "error.log"}), errorLevel)

	logger := zap.New(
		zapcore.NewTee(infoCore, errorCore),
		options...,
	)

	return &CustomLogger{logger: logger}, nil
}

// CustomFileWriter is a simple file writer that satisfies the zapcore.WriteSyncer interface
type CustomFileWriter struct {
	filepath string
}

func (w *CustomFileWriter) Sync() error {
	return nil
}

func (w *CustomFileWriter) Write(p []byte) (n int, err error) {
	timestamp := time.Now().Format("2006-01-02 15:04:05")
	logEntry := append([]byte(timestamp+" "), p...)

	file, err := os.OpenFile(w.filepath, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err != nil {
		return 0, err
	}
	defer func(file *os.File) {
		err := file.Close()
		if err != nil {
			log.Println(err)
		}
	}(file)

	return file.Write(logEntry)
}

func (l *CustomLogger) Debug(msg string) {
	l.logger.Debug(msg)
}

func (l *CustomLogger) Info(msg string) {
	l.logger.Info(msg)
}

func (l *CustomLogger) Infof(template string, args ...interface{}) {
	l.logger.Sugar().Infof(template, args...)
}

func (l *CustomLogger) Warn(msg string) {
	l.logger.Warn(msg)
}

func (l *CustomLogger) Warnf(msg string, args ...interface{}) {
	l.logger.Sugar().Warnf(msg, args...)
}

func (l *CustomLogger) Error(msg string) {
	l.logger.Error(msg)
}

func (l *CustomLogger) Errorf(template string, args ...interface{}) {
	l.logger.Sugar().Errorf(template, args...)
}
