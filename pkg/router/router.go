package router

import (
	"github.com/gorilla/mux"
	ri "gitlab.com/Russi2020QuizGroup/game-svc/pkg/router/interfaces"
	sr "gitlab.com/Russi2020QuizGroup/game-svc/pkg/router/types"
	"net/http"
)

var _ ri.RouterInterface = (*GameRouter)(nil)

type GameRouter struct {
	router *mux.Router
}

// New создает новый инстанс роутера
func New() *GameRouter {
	return &GameRouter{
		router: mux.NewRouter(),
	}
}

// Router получает роутер
func (qr *GameRouter) Router() http.Handler {
	return qr.router
}

// Endpoints endpoints Регистрация обработчиков API.
func (qr *GameRouter) Endpoints(routes sr.Routes, middleware []mux.MiddlewareFunc) {
	qr.MakeHandleFunctions(routes, middleware)
}

// MakeHandleFunctions создает хендлеры из рутов Route
func (qr *GameRouter) MakeHandleFunctions(routes sr.Routes, middleware []mux.MiddlewareFunc) {
	qr.router.Use(middleware...)
	for _, route := range routes {
		qr.router.HandleFunc(route.Pattern, route.HandlerFunc).Methods(route.Methods...)
	}
}
