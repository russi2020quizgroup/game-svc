package types

import "net/http"

type Routes []Route

type Route struct {
	Name        string
	Methods     []string
	Pattern     string
	HandlerFunc func(http.ResponseWriter, *http.Request)
}
