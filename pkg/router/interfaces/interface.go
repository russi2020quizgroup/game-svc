package interfaces

import (
	"github.com/gorilla/mux"
	rt "gitlab.com/Russi2020QuizGroup/game-svc/pkg/router/types"
	"net/http"
)

type RouterInterface interface {
	Router() http.Handler
	Endpoints(routes rt.Routes, middleware []mux.MiddlewareFunc)
	MakeHandleFunctions(routes rt.Routes, middleware []mux.MiddlewareFunc)
}
