package custom_types

import (
	"database/sql"
	"encoding/json"
)

type NullInt64 struct {
	sql.NullInt64
}

func (ni *NullInt64) UnmarshalJSON(b []byte) error {
	if string(b) == "null" {
		ni.Valid = false
		return nil
	}

	var i int64
	if err := json.Unmarshal(b, &i); err != nil {
		// returns error if the JSON is not in the expected format
		return err
	}
	ni.Int64 = i
	ni.Valid = true

	return nil
}

func (ni *NullInt64) Scan(value interface{}) error {
	var i int64
	if err := convertAssign(&i, value); err != nil {
		return err
	}

	if value == nil {
		ni.Valid = false
	} else {
		ni.Int64 = i
		ni.Valid = true
	}

	return nil
}
