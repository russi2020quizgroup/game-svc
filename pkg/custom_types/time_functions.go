package custom_types

import (
	"database/sql/driver"
	"fmt"
	"strings"
	"time"
)

const (
	dateLayout = "2006-01-02"
	timeLayout = "2006-01-02 15:04:05"
)

type CustomTime struct {
	Time time.Time
}

func (ct *CustomTime) UnmarshalJSON(b []byte) (err error) {
	s := strings.Trim(string(b), "\"")
	nt, err := time.Parse(dateLayout, s)
	*ct = CustomTime{nt}
	return
}

func (ct *CustomTime) MarshalJSON() ([]byte, error) {
	return ct.ToByte(), nil
}

func (ct *CustomTime) ToByte() []byte {
	return []byte(ct.Time.Format(dateLayout))
}

func (ct *CustomTime) ToTime() time.Time {
	return ct.Time.Local()
}

func (ct *CustomTime) String() string {
	return ct.ToTime().Format(dateLayout)
}

func (ct *CustomTime) Value() (driver.Value, error) {
	return ct.Time, nil
}

func (ct *CustomTime) Scan(value interface{}) error {
	switch v := value.(type) {
	case time.Time:
		*ct = CustomTime{v}
		return nil
	case []byte:
		// Postgres' "date" type values come as bytes from the driver
		strV := string(v)
		t, err := time.Parse("2006-01-02", strV)
		if err != nil {
			return err
		}
		*ct = CustomTime{t}
		return nil
	default:
		return fmt.Errorf("can not convert %v to timestamp", value)
	}
}

type NullTime struct {
	Time  time.Time
	Valid bool
}

func (nt *NullTime) UnmarshalJSON(b []byte) (err error) {
	s := strings.Trim(string(b), "\"")
	t, err := time.Parse(timeLayout, s) // Use "timeLayout" that includes hours, minutes and seconds
	if err == nil {
		nt.Time = t
		nt.Valid = true
	}
	return
}

func (nt *NullTime) MarshalJSON() ([]byte, error) {
	if nt.Valid {
		return nt.Time.MarshalJSON()
	}
	return []byte("null"), nil
}

func (nt *NullTime) ToByte() []byte {
	return []byte(nt.Time.Format(timeLayout))
}

func (nt *NullTime) String() string {
	return nt.ToTime().Format(dateLayout)
}

func (nt *NullTime) ToTime() time.Time {
	return nt.Time
}

func (nt *NullTime) Scan(value interface{}) error {
	if value == nil {
		*nt = NullTime{Time: time.Time{}, Valid: false}
		return nil
	}
	switch v := value.(type) {
	case time.Time:
		*nt = NullTime{Time: v, Valid: true}
	case []byte:
		t, err := time.Parse("2006-01-02 15:04:05", string(v))
		if err != nil {
			*nt = NullTime{Time: time.Time{}, Valid: false}
			return fmt.Errorf("unable to parse time: %v", err)
		}
		*nt = NullTime{Time: t, Valid: true}
	case string:
		t, err := time.Parse("2006-01-02 15:04:05", v)
		if err != nil {
			*nt = NullTime{Time: time.Time{}, Valid: false}
			return fmt.Errorf("unable to parse time: %v", err)
		}
		*nt = NullTime{Time: t, Valid: true}
	default:
		return fmt.Errorf("unsupported type for NullTime Scan method %T", v)
	}
	return nil
}
