package custom_types

import (
	"fmt"
	"time"
)

func convertAssign(dest, src interface{}) error {
	switch src := src.(type) {
	case int64:
		*dest.(*int64) = src
		return nil
	case time.Time:
		*dest.(*time.Time) = src
		return nil
	default:
		return fmt.Errorf("Incompatible type for %T", src)
	}
}
