package main

import "gitlab.com/Russi2020QuizGroup/game-svc/internal/bootstrap"

func main() {
	app := bootstrap.Setup()
	app.Run()
}
