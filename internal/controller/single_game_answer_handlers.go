package controller

import (
	"context"
	"encoding/json"
	"github.com/gorilla/mux"
	"gitlab.com/Russi2020QuizGroup/game-svc/internal/controller/interfaces"
	"gitlab.com/Russi2020QuizGroup/game-svc/internal/models"
	apiModels "gitlab.com/Russi2020QuizGroup/game-svc/internal/models"
	"net/http"
	"strconv"
	"time"
)

var _ interfaces.SingleGameAnswerController = (*Controller)(nil)

func (c *Controller) AddSingleGameAnswerHandler(w http.ResponseWriter, r *http.Request) {
	var req apiModels.AddSingleGameAnswerRequest
	ctx := r.Context()
	if c.requestTimeOut > 0 {
		var cancel context.CancelFunc

		ctx, cancel = context.WithTimeout(ctx, time.Duration(c.requestTimeOut)*time.Second)
		defer cancel()
	}

	contentType := r.Header.Get("Content-Type")
	switch contentType {
	case "application/json":
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			c.Logger.Error(err.Error())
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
	default:
		http.Error(w, "Unsupported content type", http.StatusBadRequest)
		return
	}

	var resp apiModels.AddSingleGameAnswerResponse
	singleGameAnswer := models.SingleGameAnswer{
		PlayerID:        req.PlayerID,
		GameStatisticId: req.GameStatisticId,
		RoundId:         req.RoundId,
		QuestionId:      req.QuestionId,
		AnswerID:        req.AnswerID,
		IsCorrect:       req.IsCorrect,
	}

	err := c.service.AddSingleGameAnswer(ctx, singleGameAnswer)

	if err != nil {
		c.Logger.Error(err.Error())
		resp.Status = http.StatusInternalServerError
		err = json.NewEncoder(w).Encode(resp)

		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	resp = models.AddSingleGameAnswerResponse{
		Status: http.StatusCreated,
	}

	err = json.NewEncoder(w).Encode(resp)

	if err != nil {
		c.Logger.Error(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (c *Controller) SingleGameAnswersByGameIdHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	if c.requestTimeOut > 0 {
		var cancel context.CancelFunc

		ctx, cancel = context.WithTimeout(ctx, time.Duration(c.requestTimeOut)*time.Second)
		defer cancel()
	}

	vars := mux.Vars(r)
	gameIdString := vars["game_id"]

	gameId, err := strconv.Atoi(gameIdString)
	if err != nil {
		c.Logger.Error(err.Error())
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	var resp apiModels.SingleGameAnswersByGameIdResponse

	singleGameAnswers, err := c.service.SingleGameAnswersByGameId(ctx, uint(gameId))

	if err != nil {
		c.Logger.Error(err.Error())
		resp.Status = http.StatusInternalServerError
		err = json.NewEncoder(w).Encode(resp)

		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	resp = models.SingleGameAnswersByGameIdResponse{
		Status:            http.StatusOK,
		SingleGameAnswers: singleGameAnswers,
	}

	err = json.NewEncoder(w).Encode(resp)

	if err != nil {
		c.Logger.Error(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (c *Controller) UpdateSingleGameAnswerHandler(w http.ResponseWriter, r *http.Request) {
	var req apiModels.UpdateSingleGameAnswerRequest
	ctx := r.Context()
	if c.requestTimeOut > 0 {
		var cancel context.CancelFunc

		ctx, cancel = context.WithTimeout(ctx, time.Duration(c.requestTimeOut)*time.Second)
		defer cancel()
	}

	contentType := r.Header.Get("Content-Type")
	switch contentType {
	case "application/json":
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			c.Logger.Error(err.Error())
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
	default:
		http.Error(w, "Unsupported content type", http.StatusBadRequest)
		return
	}

	var resp apiModels.UpdateSingleGameAnswerResponse
	singleGameAnswer := models.SingleGameAnswer{
		Id:        req.Id,
		AnswerID:  req.AnswerID,
		IsCorrect: req.IsCorrect,
	}

	err := c.service.UpdateSingleGameAnswer(ctx, singleGameAnswer)

	if err != nil {
		c.Logger.Error(err.Error())
		resp.Status = http.StatusInternalServerError
		err = json.NewEncoder(w).Encode(resp)

		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	resp = models.UpdateSingleGameAnswerResponse{
		Status: http.StatusOK,
	}

	err = json.NewEncoder(w).Encode(resp)

	if err != nil {
		c.Logger.Error(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (c *Controller) DeleteSingleGameAnswerHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	if c.requestTimeOut > 0 {
		var cancel context.CancelFunc

		ctx, cancel = context.WithTimeout(ctx, time.Duration(c.requestTimeOut)*time.Second)
		defer cancel()
	}

	vars := mux.Vars(r)
	idString := vars["id"]
	id, err := strconv.Atoi(idString)
	if err != nil {
		c.Logger.Error(err.Error())
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	var resp apiModels.DeleteSingleGameAnswerResponse

	err = c.service.DeleteSingleGameAnswer(ctx, uint(id))

	if err != nil {
		c.Logger.Error(err.Error())
		resp.Status = http.StatusInternalServerError
		err = json.NewEncoder(w).Encode(resp)

		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	resp = models.DeleteSingleGameAnswerResponse{
		Status: http.StatusOK,
	}

	err = json.NewEncoder(w).Encode(resp)

	if err != nil {
		c.Logger.Error(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
