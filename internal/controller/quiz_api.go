package controller

import (
	"context"
	"encoding/json"
	"fmt"
	"gitlab.com/Russi2020QuizGroup/game-svc/internal/controller/interfaces"
	apiModels "gitlab.com/Russi2020QuizGroup/game-svc/internal/models"
	"io"
	"net/http"
	"strconv"
	"time"
)

var _ interfaces.QuizAPIController = (*Controller)(nil)

func (c *Controller) CategoriesHandler(w http.ResponseWriter, r *http.Request) {
	var resp apiModels.CategoriesResponse
	ctx := r.Context()
	if c.requestTimeOut > 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, time.Duration(c.requestTimeOut)*time.Second)
		defer cancel()
	}

	req, err := http.Get(c.QuizAPIPath + "/categories")

	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			c.Logger.Error(err.Error())
			resp.Status = http.StatusInternalServerError
			err = json.NewEncoder(w).Encode(resp)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}(req.Body)

	if err != nil {
		c.Logger.Error(err.Error())
		resp.Status = http.StatusInternalServerError
		err = json.NewEncoder(w).Encode(resp)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	body, err := io.ReadAll(req.Body)

	if err != nil {
		c.Logger.Error(err.Error())
		resp.Status = http.StatusInternalServerError
		err = json.NewEncoder(w).Encode(resp)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = json.Unmarshal(body, &resp)

	if err != nil {
		c.Logger.Error(err.Error())
		resp.Status = http.StatusInternalServerError
		err = json.NewEncoder(w).Encode(resp)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = json.NewEncoder(w).Encode(resp)

	if err != nil {
		c.Logger.Error(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (c *Controller) ThemesHandler(w http.ResponseWriter, r *http.Request) {
	var resp apiModels.ThemesResponse
	ctx := r.Context()
	if c.requestTimeOut > 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, time.Duration(c.requestTimeOut)*time.Second)
		defer cancel()
	}

	categoryIdString := r.URL.Query().Get("category_id")
	categoryId, err := strconv.Atoi(categoryIdString)

	if err != nil {
		c.Logger.Error(err.Error())
		resp.Status = http.StatusBadRequest
		err = json.NewEncoder(w).Encode(resp)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	req, err := http.Get(fmt.Sprintf(c.QuizAPIPath+"/theme/%d", categoryId))
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			c.Logger.Error(err.Error())
			resp.Status = http.StatusInternalServerError
			err = json.NewEncoder(w).Encode(resp)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}(req.Body)

	if err != nil {
		c.Logger.Error(err.Error())
		resp.Status = http.StatusInternalServerError
		err = json.NewEncoder(w).Encode(resp)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	body, err := io.ReadAll(req.Body)

	if err != nil {
		c.Logger.Error(err.Error())
		resp.Status = http.StatusInternalServerError
		err = json.NewEncoder(w).Encode(resp)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = json.Unmarshal(body, &resp)

	if err != nil {
		c.Logger.Error(err.Error())
		resp.Status = http.StatusInternalServerError
		err = json.NewEncoder(w).Encode(resp)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = json.NewEncoder(w).Encode(resp)

	if err != nil {
		c.Logger.Error(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (c *Controller) GameThemesControllerHandler(w http.ResponseWriter, r *http.Request) {
	var resp apiModels.GameThemesByThemeIdResponse
	ctx := r.Context()
	if c.requestTimeOut > 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, time.Duration(c.requestTimeOut)*time.Second)
		defer cancel()
	}

	themeIdString := r.URL.Query().Get("theme_id")

	if themeIdString == "" {
		c.Logger.Error("Missing 'theme_id' parameter")
		http.Error(w, "Missing 'theme_id' parameter", http.StatusBadRequest)
		return
	}

	themeId, err := strconv.Atoi(themeIdString)

	if err != nil {
		c.Logger.Error(err.Error())
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	req, err := http.Get(fmt.Sprintf(c.QuizAPIPath+"/game-theme/%d", themeId))

	if err != nil {
		c.Logger.Error(err.Error())
		resp.Status = http.StatusInternalServerError
		err = json.NewEncoder(w).Encode(resp)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			c.Logger.Error(err.Error())
			resp.Status = http.StatusInternalServerError
			err = json.NewEncoder(w).Encode(resp)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}(req.Body)

	body, err := io.ReadAll(req.Body)

	if err != nil {
		c.Logger.Error(err.Error())
		resp.Status = http.StatusInternalServerError
		err = json.NewEncoder(w).Encode(resp)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = json.Unmarshal(body, &resp)

	if err != nil {
		c.Logger.Error(err.Error())
		resp.Status = http.StatusInternalServerError
		err = json.NewEncoder(w).Encode(resp)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = json.NewEncoder(w).Encode(resp)

	if err != nil {
		c.Logger.Error(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (c *Controller) GameByGameThemeIdHandler(w http.ResponseWriter, r *http.Request) {
	var resp apiModels.GameByGameThemeIdResponse
	ctx := r.Context()
	if c.requestTimeOut > 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, time.Duration(c.requestTimeOut)*time.Second)
		defer cancel()
	}

	time.Sleep(4 * time.Second)

	gameThemeIdString := r.URL.Query().Get("game_theme_id")

	if gameThemeIdString == "" {
		c.Logger.Error("Missing 'game_theme_id' parameter")
		http.Error(w, "Missing 'game_theme_id' parameter", http.StatusBadRequest)
		return
	}

	gameThemeId, err := strconv.Atoi(gameThemeIdString)

	if err != nil {
		c.Logger.Error(err.Error())
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	req, err := http.Get(fmt.Sprintf(c.QuizAPIPath+"/game/game_theme/%d", gameThemeId))

	if err != nil {
		c.Logger.Error(err.Error())
		resp.Status = http.StatusInternalServerError
		err = json.NewEncoder(w).Encode(resp)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			c.Logger.Error(err.Error())
			resp.Status = http.StatusInternalServerError
			err = json.NewEncoder(w).Encode(resp)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}(req.Body)

	body, err := io.ReadAll(req.Body)

	if err != nil {
		c.Logger.Error(err.Error())
		resp.Status = http.StatusInternalServerError
		err = json.NewEncoder(w).Encode(resp)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = json.Unmarshal(body, &resp)

	if err != nil {
		c.Logger.Error(err.Error())
		resp.Status = http.StatusInternalServerError
		err = json.NewEncoder(w).Encode(resp)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = json.NewEncoder(w).Encode(resp)

	if err != nil {
		c.Logger.Error(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
