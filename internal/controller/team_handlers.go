package controller

import (
	"context"
	"encoding/json"
	"github.com/gorilla/mux"
	"gitlab.com/Russi2020QuizGroup/game-svc/internal/controller/interfaces"
	"gitlab.com/Russi2020QuizGroup/game-svc/internal/models"
	apiModels "gitlab.com/Russi2020QuizGroup/game-svc/internal/models"
	"net/http"
	"strconv"
	"time"
)

var _ interfaces.TeamController = (*Controller)(nil)

func (c *Controller) AddTeamHandler(w http.ResponseWriter, r *http.Request) {
	var req apiModels.CreateTeamRequest
	ctx := r.Context()
	if c.requestTimeOut > 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, time.Duration(c.requestTimeOut)*time.Second)
		defer cancel()
	}

	contentType := r.Header.Get("Content-Type")
	switch contentType {
	case "application/json":
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			c.Logger.Error(err.Error())
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
	default:
		http.Error(w, "Unsupported content type", http.StatusBadRequest)
		return
	}

	var resp apiModels.CreateTeamResponse
	team := models.Team{Name: req.Name, Players: req.Players}
	err := c.service.AddTeam(ctx, team)

	if err != nil {
		c.Logger.Error(err.Error())
		resp.Status = http.StatusInternalServerError
		err = json.NewEncoder(w).Encode(resp)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	resp = apiModels.CreateTeamResponse{
		Status: http.StatusCreated,
	}

	err = json.NewEncoder(w).Encode(resp)

	if err != nil {
		c.Logger.Error(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (c *Controller) AddTeamMemberHandler(w http.ResponseWriter, r *http.Request) {
	var req apiModels.AddTeamMemberRequest
	ctx := r.Context()
	if c.requestTimeOut > 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, time.Duration(c.requestTimeOut)*time.Second)
		defer cancel()
	}

	contentType := r.Header.Get("Content-Type")
	switch contentType {
	case "application/json":
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			c.Logger.Error(err.Error())
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
	default:
		http.Error(w, "Unsupported content type", http.StatusBadRequest)
		return
	}

	var resp apiModels.AddTeamMemberResponse

	err := c.service.AddTeamMember(ctx, req.PlayerId, req.TeamId)

	if err != nil {
		c.Logger.Error(err.Error())
		resp.Status = http.StatusInternalServerError
		err = json.NewEncoder(w).Encode(resp)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	resp = apiModels.AddTeamMemberResponse{
		Status: http.StatusCreated,
	}

	err = json.NewEncoder(w).Encode(resp)

	if err != nil {
		c.Logger.Error(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (c *Controller) TeamsByNameHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	if c.requestTimeOut > 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, time.Duration(c.requestTimeOut)*time.Second)
		defer cancel()
	}

	vars := mux.Vars(r)
	name := vars["name"]

	if name == "" {
		http.Error(w, "Name is required", http.StatusBadRequest)
		return
	}

	var resp apiModels.TeamByNameResponse

	teams, err := c.service.TeamsByName(ctx, name)

	if err != nil {
		c.Logger.Error(err.Error())
		resp.Status = http.StatusInternalServerError
		err = json.NewEncoder(w).Encode(resp)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	resp = apiModels.TeamByNameResponse{
		Status: http.StatusOK,
		Team:   teams,
	}

	err = json.NewEncoder(w).Encode(resp)

	if err != nil {
		c.Logger.Error(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (c *Controller) AllTeamMembersHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	if c.requestTimeOut > 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, time.Duration(c.requestTimeOut)*time.Second)

		defer cancel()
	}

	vars := mux.Vars(r)
	teamIdString := vars["team_id"]

	teamId, err := strconv.Atoi(teamIdString)
	if err != nil {
		c.Logger.Error(err.Error())
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	var resp apiModels.AllTeamMembersResponse

	players, err := c.service.AllTeamMembers(ctx, uint(teamId))

	if err != nil {
		c.Logger.Error(err.Error())
		resp.Status = http.StatusInternalServerError
		err = json.NewEncoder(w).Encode(resp)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	resp = apiModels.AllTeamMembersResponse{
		Status:  http.StatusOK,
		Players: players,
	}

	err = json.NewEncoder(w).Encode(resp)

	if err != nil {
		c.Logger.Error(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (c *Controller) TeamsHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	if c.requestTimeOut > 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, time.Duration(c.requestTimeOut)*time.Second)
		defer cancel()

	}

	var resp apiModels.TeamsResponse

	teams, err := c.service.Teams(ctx)

	if err != nil {
		c.Logger.Error(err.Error())
		resp.Status = http.StatusInternalServerError
		err = json.NewEncoder(w).Encode(resp)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	resp = apiModels.TeamsResponse{
		Status: http.StatusOK,
		Teams:  teams,
	}

	err = json.NewEncoder(w).Encode(resp)

	if err != nil {
		c.Logger.Error(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (c *Controller) UpdateTeamNameHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	if c.requestTimeOut > 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, time.Duration(c.requestTimeOut)*time.Second)

		defer cancel()
	}

	vars := mux.Vars(r)
	idString := vars["id"]
	id, err := strconv.Atoi(idString)
	if err != nil {
		c.Logger.Error(err.Error())
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	name := vars["name"]

	var resp apiModels.UpdateTeamNameResponse
	team := models.Team{Id: uint(id), Name: name}

	err = c.service.UpdateTeamName(ctx, team)

	if err != nil {
		c.Logger.Error(err.Error())
		resp.Status = http.StatusInternalServerError
		err = json.NewEncoder(w).Encode(resp)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (c *Controller) DeleteTeamHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	if c.requestTimeOut > 0 {
		var cancel context.CancelFunc

		ctx, cancel = context.WithTimeout(ctx, time.Duration(c.requestTimeOut)*time.Second)
		defer cancel()
	}

	vars := mux.Vars(r)
	idString := vars["id"]
	id, err := strconv.Atoi(idString)
	if err != nil {
		c.Logger.Error(err.Error())
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	var resp apiModels.DeleteTeamResponse

	err = c.service.DeleteTeam(ctx, uint(id))

	if err != nil {
		c.Logger.Error(err.Error())
		resp.Status = http.StatusInternalServerError
		err = json.NewEncoder(w).Encode(resp)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
