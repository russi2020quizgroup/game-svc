package controller

import (
	rt "gitlab.com/Russi2020QuizGroup/game-svc/pkg/router/types"
	"net/http"
)

func (c *Controller) AddRoutes() {
	// Player routes

	{
		route := rt.Route{
			Name:    "add_player",
			Pattern: "/player",
			HandlerFunc: func(w http.ResponseWriter, r *http.Request) {
				c.AddPlayerHandler(w, r)
			},
			Methods: []string{http.MethodPost, http.MethodOptions},
		}
		c.routes = append(c.routes, route)
	}

	{
		route := rt.Route{
			Name:    "player_by_email",
			Pattern: "/player/{email}",
			HandlerFunc: func(w http.ResponseWriter, r *http.Request) {
				c.PlayerByEmailHandler(w, r)
			},
			Methods: []string{http.MethodGet, http.MethodOptions},
		}
		c.routes = append(c.routes, route)
	}

	{
		route := rt.Route{
			Name:    "player_by_username",
			Pattern: "/player/username/{username}",
			HandlerFunc: func(w http.ResponseWriter, r *http.Request) {
				c.PlayersByUsernameHandler(w, r)
			},
			Methods: []string{http.MethodGet, http.MethodOptions},
		}
		c.routes = append(c.routes, route)
	}

	{
		route := rt.Route{
			Name:    "players",
			Pattern: "/players",
			HandlerFunc: func(w http.ResponseWriter, r *http.Request) {
				c.PlayersHandler(w, r)
			},
			Methods: []string{http.MethodGet, http.MethodOptions},
		}
		c.routes = append(c.routes, route)
	}

	{
		route := rt.Route{
			Name:    "update_player_username",
			Pattern: "/player/username/{id}/{username}",
			HandlerFunc: func(w http.ResponseWriter, r *http.Request) {
				c.UpdatePlayerUsernameHandler(w, r)
			},
			Methods: []string{http.MethodPut, http.MethodOptions},
		}
		c.routes = append(c.routes, route)
	}

	{
		route := rt.Route{
			Name:    "update_player_email",
			Pattern: "/player/email/{id}/{email}",
			HandlerFunc: func(w http.ResponseWriter, r *http.Request) {
				c.UpdatePlayerEmailHandler(w, r)
			},
			Methods: []string{http.MethodPut, http.MethodOptions},
		}
		c.routes = append(c.routes, route)
	}

	{
		route := rt.Route{
			Name:    "delete_player",
			Pattern: "/player/{id}",
			HandlerFunc: func(w http.ResponseWriter, r *http.Request) {
				c.DeletePlayerHandler(w, r)
			},
			Methods: []string{http.MethodDelete, http.MethodOptions},
		}
		c.routes = append(c.routes, route)
	}

	// Team routes

	{
		route := rt.Route{
			Name:    "add_team",
			Pattern: "/team",
			HandlerFunc: func(w http.ResponseWriter, r *http.Request) {
				c.AddTeamHandler(w, r)
			},
			Methods: []string{http.MethodPost, http.MethodOptions},
		}
		c.routes = append(c.routes, route)
	}

	{
		route := rt.Route{
			Name:    "add_team_member",
			Pattern: "/team/member",
			HandlerFunc: func(w http.ResponseWriter, r *http.Request) {
				c.AddTeamMemberHandler(w, r)
			},
			Methods: []string{http.MethodPost, http.MethodOptions},
		}
		c.routes = append(c.routes, route)
	}

	{
		route := rt.Route{
			Name:    "teams_by_name",
			Pattern: "/team/{name}",
			HandlerFunc: func(w http.ResponseWriter, r *http.Request) {
				c.TeamsByNameHandler(w, r)
			},
			Methods: []string{http.MethodGet, http.MethodOptions},
		}
		c.routes = append(c.routes, route)
	}

	{
		route := rt.Route{
			Name:    "teams",
			Pattern: "/teams",
			HandlerFunc: func(w http.ResponseWriter, r *http.Request) {
				c.TeamsHandler(w, r)
			},
			Methods: []string{http.MethodGet, http.MethodOptions},
		}
		c.routes = append(c.routes, route)
	}

	{
		route := rt.Route{
			Name:    "all_team_members",
			Pattern: "/team/members/{team_id}",
			HandlerFunc: func(w http.ResponseWriter, r *http.Request) {
				c.AllTeamMembersHandler(w, r)
			},
			Methods: []string{http.MethodGet, http.MethodOptions},
		}
		c.routes = append(c.routes, route)
	}

	{
		route := rt.Route{
			Name:    "update_team_name",
			Pattern: "/team/name/{id}/{name}",
			HandlerFunc: func(w http.ResponseWriter, r *http.Request) {
				c.UpdateTeamNameHandler(w, r)
			},
			Methods: []string{http.MethodPut, http.MethodOptions},
		}
		c.routes = append(c.routes, route)
	}

	{
		route := rt.Route{
			Name:    "delete_team",
			Pattern: "/team/{id}",
			HandlerFunc: func(w http.ResponseWriter, r *http.Request) {
				c.DeleteTeamHandler(w, r)
			},
			Methods: []string{http.MethodDelete, http.MethodOptions},
		}
		c.routes = append(c.routes, route)
	}

	// Game answer routes

	{
		route := rt.Route{
			Name:    "single_game_answer",
			Pattern: "/single_game_answers/add",
			HandlerFunc: func(w http.ResponseWriter, r *http.Request) {
				c.AddSingleGameAnswerHandler(w, r)
			},
			Methods: []string{http.MethodPost, http.MethodOptions},
		}
		c.routes = append(c.routes, route)
	}

	{
		route := rt.Route{
			Name:    "single_game_answers_by_game_id",
			Pattern: "/single_game_answers/{game_id}",
			HandlerFunc: func(w http.ResponseWriter, r *http.Request) {
				c.SingleGameAnswersByGameIdHandler(w, r)
			},
			Methods: []string{http.MethodGet, http.MethodOptions},
		}
		c.routes = append(c.routes, route)
	}

	{
		route := rt.Route{
			Name:    "update_single_game_answer",
			Pattern: "/single_game_answers/update",
			HandlerFunc: func(w http.ResponseWriter, r *http.Request) {
				c.UpdateSingleGameAnswerHandler(w, r)
			},
			Methods: []string{http.MethodPut, http.MethodOptions},
		}
		c.routes = append(c.routes, route)
	}

	{
		route := rt.Route{
			Name:    "delete_single_game_answer",
			Pattern: "/single_game_answers/delete/{id}",
			HandlerFunc: func(w http.ResponseWriter, r *http.Request) {
				c.DeleteSingleGameAnswerHandler(w, r)
			},
			Methods: []string{http.MethodDelete, http.MethodOptions},
		}
		c.routes = append(c.routes, route)
	}

	// Team game answer routes

	{
		route := rt.Route{
			Name:    "team_game_answer",
			Pattern: "/team_game_answers/add",
			HandlerFunc: func(w http.ResponseWriter, r *http.Request) {
				c.AddTeamGameAnswerHandler(w, r)
			},
			Methods: []string{http.MethodPost, http.MethodOptions},
		}
		c.routes = append(c.routes, route)
	}

	{
		route := rt.Route{
			Name:    "team_game_answers_by_game_id",
			Pattern: "/team_game_answers/{game_id}",
			HandlerFunc: func(w http.ResponseWriter, r *http.Request) {
				c.TeamGameAnswersByGameIdHandler(w, r)
			},
			Methods: []string{http.MethodGet, http.MethodOptions},
		}
		c.routes = append(c.routes, route)
	}

	{
		route := rt.Route{
			Name:    "update_team_game_answer",
			Pattern: "/team_game_answers/update",
			HandlerFunc: func(w http.ResponseWriter, r *http.Request) {
				c.UpdateTeamGameAnswerHandler(w, r)
			},
			Methods: []string{http.MethodPut, http.MethodOptions},
		}
		c.routes = append(c.routes, route)
	}

	{
		route := rt.Route{
			Name:    "delete_team_game_answer",
			Pattern: "/team_game_answers/delete/{id}",
			HandlerFunc: func(w http.ResponseWriter, r *http.Request) {
				c.DeleteTeamGameAnswerHandler(w, r)
			},
			Methods: []string{http.MethodDelete, http.MethodOptions},
		}
		c.routes = append(c.routes, route)
	}

	// Statistic routes

	{
		route := rt.Route{
			Name:    "add_game_statistic",
			Pattern: "/game_statistic",
			HandlerFunc: func(w http.ResponseWriter, r *http.Request) {
				c.AddGameStatisticHandler(w, r)
			},
			Methods: []string{http.MethodPost, http.MethodOptions},
		}
		c.routes = append(c.routes, route)
	}

	{
		route := rt.Route{
			Name:    "game_statistic_by_game_id",
			Pattern: "/game_statistic/{game_id}",
			HandlerFunc: func(w http.ResponseWriter, r *http.Request) {
				c.GameStatisticByGameIDHandler(w, r)
			},
			Methods: []string{http.MethodGet, http.MethodOptions},
		}
		c.routes = append(c.routes, route)
	}

	{
		route := rt.Route{
			Name:    "game_statistic_by_player_id",
			Pattern: "/game_statistic/player/{player_id}",
			HandlerFunc: func(w http.ResponseWriter, r *http.Request) {
				c.GameStatisticByPlayerIDHandler(w, r)
			},
			Methods: []string{http.MethodGet, http.MethodOptions},
		}
		c.routes = append(c.routes, route)
	}

	{
		route := rt.Route{
			Name:    "game_statistic_by_team_id",
			Pattern: "/game_statistic/team/{team_id}",
			HandlerFunc: func(w http.ResponseWriter, r *http.Request) {
				c.GameStatisticByTeamIDHandler(w, r)
			},
			Methods: []string{http.MethodGet, http.MethodOptions},
		}
		c.routes = append(c.routes, route)
	}

	// QuizAPI routes

	{
		route := rt.Route{
			Name:    "Get_Categories",
			Pattern: "/categories",
			HandlerFunc: func(w http.ResponseWriter, r *http.Request) {
				c.CategoriesHandler(w, r)
			},
			Methods: []string{http.MethodGet, http.MethodOptions},
		}
		c.routes = append(c.routes, route)
	}

	{
		route := rt.Route{
			Name:    "Get_Themes",
			Pattern: "/themes",
			HandlerFunc: func(w http.ResponseWriter, r *http.Request) {
				c.ThemesHandler(w, r)
			},
			Methods: []string{http.MethodGet, http.MethodOptions},
		}
		c.routes = append(c.routes, route)
	}

	{
		route := rt.Route{
			Name:    "Get_GameThemes",
			Pattern: "/game-theme",
			HandlerFunc: func(w http.ResponseWriter, r *http.Request) {
				c.GameThemesControllerHandler(w, r)
			},
			Methods: []string{http.MethodGet, http.MethodOptions},
		}
		c.routes = append(c.routes, route)
	}

	{
		route := rt.Route{
			Name:    "Get_GameByGameThemeId",
			Pattern: "/game/game_theme",
			HandlerFunc: func(w http.ResponseWriter, r *http.Request) {
				c.GameByGameThemeIdHandler(w, r)
			},
			Methods: []string{http.MethodGet, http.MethodOptions},
		}
		c.routes = append(c.routes, route)
	}
}

func (c *Controller) Routes() rt.Routes {
	return c.routes
}
