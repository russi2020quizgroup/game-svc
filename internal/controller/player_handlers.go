package controller

import (
	"context"
	"encoding/json"
	"github.com/gorilla/mux"
	"gitlab.com/Russi2020QuizGroup/game-svc/internal/controller/interfaces"
	"gitlab.com/Russi2020QuizGroup/game-svc/internal/models"
	apiModels "gitlab.com/Russi2020QuizGroup/game-svc/internal/models"
	"net/http"
	"strconv"
	"time"
)

var _ interfaces.PlayerController = (*Controller)(nil)

func (c *Controller) AddPlayerHandler(w http.ResponseWriter, r *http.Request) {
	var req apiModels.CreatePlayerRequest
	ctx := r.Context()
	if c.requestTimeOut > 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, time.Duration(c.requestTimeOut)*time.Second)
		defer cancel()
	}

	contentType := r.Header.Get("Content-Type")
	switch contentType {
	case "application/json":
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			c.Logger.Error(err.Error())
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
	default:
		http.Error(w, "Unsupported content type", http.StatusBadRequest)
		return
	}

	var resp apiModels.CreatePlayerResponse

	err := c.service.AddPlayer(ctx, req.PlayerInstance)

	if err != nil {
		c.Logger.Error(err.Error())
		resp.Status = http.StatusInternalServerError
		err = json.NewEncoder(w).Encode(resp)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	resp = apiModels.CreatePlayerResponse{
		Status: http.StatusCreated,
	}

	err = json.NewEncoder(w).Encode(resp)

	if err != nil {
		c.Logger.Error(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (c *Controller) PlayerByEmailHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	if c.requestTimeOut > 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, time.Duration(c.requestTimeOut)*time.Second)
		defer cancel()
	}

	vars := mux.Vars(r)
	email := vars["email"]

	if email == "" {
		http.Error(w, "Email is required", http.StatusBadRequest)
		return
	}

	var resp apiModels.PlayerByEmailResponse

	player, err := c.service.PlayerByEmail(ctx, email)

	if err != nil {
		c.Logger.Error(err.Error())
		resp.Status = http.StatusInternalServerError
		err = json.NewEncoder(w).Encode(resp)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	resp = apiModels.PlayerByEmailResponse{
		Status: http.StatusOK,
		Player: player,
	}

	err = json.NewEncoder(w).Encode(resp)

	if err != nil {
		c.Logger.Error(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (c *Controller) PlayersByUsernameHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	if c.requestTimeOut > 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, time.Duration(c.requestTimeOut)*time.Second)
		defer cancel()
	}

	vars := mux.Vars(r)
	username := vars["username"]

	if username == "" {
		http.Error(w, "Username is required", http.StatusBadRequest)
		return
	}

	var resp apiModels.PlayerByUsernameResponse

	players, err := c.service.PlayersByUsername(ctx, username)

	if err != nil {
		c.Logger.Error(err.Error())
		resp.Status = http.StatusInternalServerError
		err = json.NewEncoder(w).Encode(resp)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	resp = apiModels.PlayerByUsernameResponse{
		Status:  http.StatusOK,
		Players: players,
	}

	err = json.NewEncoder(w).Encode(resp)

	if err != nil {
		c.Logger.Error(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (c *Controller) PlayersHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	if c.requestTimeOut > 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, time.Duration(c.requestTimeOut)*time.Second)
		defer cancel()
	}

	var resp apiModels.PlayersResponse

	players, err := c.service.Players(ctx)

	if err != nil {
		c.Logger.Error(err.Error())
		resp.Status = http.StatusInternalServerError
		err = json.NewEncoder(w).Encode(resp)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	resp = apiModels.PlayersResponse{
		Status:  http.StatusOK,
		Players: players,
	}

	err = json.NewEncoder(w).Encode(resp)

	if err != nil {
		c.Logger.Error(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (c *Controller) UpdatePlayerUsernameHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	if c.requestTimeOut > 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, time.Duration(c.requestTimeOut)*time.Second)
		defer cancel()
	}

	vars := mux.Vars(r)
	idString := vars["id"]
	id, err := strconv.Atoi(idString)
	if err != nil {
		c.Logger.Error(err.Error())
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	username := vars["username"]

	player := models.Player{Id: uint(id), Username: username}
	var resp apiModels.UpdatePlayerUsernameResponse

	err = c.service.UpdatePlayerUsername(ctx, player)

	if err != nil {
		c.Logger.Error(err.Error())
		resp.Status = http.StatusInternalServerError
		err = json.NewEncoder(w).Encode(resp)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (c *Controller) UpdatePlayerEmailHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	if c.requestTimeOut > 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, time.Duration(c.requestTimeOut)*time.Second)
		defer cancel()
	}

	vars := mux.Vars(r)
	idString := vars["id"]
	id, err := strconv.Atoi(idString)
	if err != nil {
		c.Logger.Error(err.Error())
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	email := vars["email"]

	player := models.Player{Id: uint(id), Email: email}
	var resp apiModels.UpdatePlayerEmailResponse

	err = c.service.UpdatePlayerEmail(ctx, player)

	if err != nil {
		c.Logger.Error(err.Error())
		resp.Status = http.StatusInternalServerError
		err = json.NewEncoder(w).Encode(resp)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (c *Controller) DeletePlayerHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	if c.requestTimeOut > 0 {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(ctx, time.Duration(c.requestTimeOut)*time.Second)
		defer cancel()
	}

	vars := mux.Vars(r)
	idString := vars["id"]
	id, err := strconv.Atoi(idString)
	if err != nil {
		c.Logger.Error(err.Error())
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	var resp apiModels.DeletePlayerResponse

	err = c.service.DeletePlayer(ctx, uint(id))

	if err != nil {
		c.Logger.Error(err.Error())
		resp.Status = http.StatusInternalServerError
		err = json.NewEncoder(w).Encode(resp)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
