package controller

import (
	"context"
	"encoding/json"
	"github.com/gorilla/mux"
	"gitlab.com/Russi2020QuizGroup/game-svc/internal/controller/interfaces"
	"gitlab.com/Russi2020QuizGroup/game-svc/internal/models"
	apiModels "gitlab.com/Russi2020QuizGroup/game-svc/internal/models"
	"net/http"
	"strconv"
	"time"
)

var _ interfaces.GameStatisticController = (*Controller)(nil)

func (c *Controller) AddGameStatisticHandler(w http.ResponseWriter, r *http.Request) {
	var req apiModels.AddGameStatisticRequest
	ctx := r.Context()
	if c.requestTimeOut > 0 {
		var cancel context.CancelFunc

		ctx, cancel = context.WithTimeout(ctx, time.Duration(c.requestTimeOut)*time.Second)
		defer cancel()
	}

	contentType := r.Header.Get("Content-Type")
	switch contentType {
	case "application/json":
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			c.Logger.Error(err.Error())
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
	default:
		http.Error(w, "Unsupported content type", http.StatusBadRequest)
		return
	}

	var resp apiModels.AddGameStatisticResponse

	err := c.service.AddGameStatistic(ctx, req.GameId, req.Date, req.StartTime)

	if err != nil {
		c.Logger.Error(err.Error())
		resp.Status = http.StatusInternalServerError
		err = json.NewEncoder(w).Encode(resp)

		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	resp = models.AddGameStatisticResponse{
		Status: http.StatusCreated,
	}

	err = json.NewEncoder(w).Encode(resp)

	if err != nil {
		c.Logger.Error(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (c *Controller) GameStatisticByGameIDHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	if c.requestTimeOut > 0 {
		var cancel context.CancelFunc

		ctx, cancel = context.WithTimeout(ctx, time.Duration(c.requestTimeOut)*time.Second)
		defer cancel()
	}

	vars := mux.Vars(r)
	gameIdString := vars["game_id"]

	gameId, err := strconv.Atoi(gameIdString)
	if err != nil {
		c.Logger.Error(err.Error())
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	var resp apiModels.GameStatisticByGameIDResponse

	gameStatistic, err := c.service.GameStatisticByGameID(ctx, uint(gameId))

	if err != nil {
		c.Logger.Error(err.Error())
		resp.Status = http.StatusInternalServerError
		err = json.NewEncoder(w).Encode(resp)

		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	resp = models.GameStatisticByGameIDResponse{
		Status:        http.StatusOK,
		GameStatistic: gameStatistic,
	}

	err = json.NewEncoder(w).Encode(resp)

	if err != nil {
		c.Logger.Error(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (c *Controller) GameStatisticByPlayerIDHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	if c.requestTimeOut > 0 {
		var cancel context.CancelFunc

		ctx, cancel = context.WithTimeout(ctx, time.Duration(c.requestTimeOut)*time.Second)

		defer cancel()
	}

	vars := mux.Vars(r)
	playerIdString := vars["player_id"]

	playerId, err := strconv.Atoi(playerIdString)
	if err != nil {
		c.Logger.Error(err.Error())

		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	var resp apiModels.GameStatisticByPlayerIDResponse

	gameStatistic, err := c.service.GameStatisticByPlayerID(ctx, uint(playerId))

	if err != nil {
		c.Logger.Error(err.Error())
		resp.Status = http.StatusInternalServerError

		err = json.NewEncoder(w).Encode(resp)

		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	resp = models.GameStatisticByPlayerIDResponse{
		Status:        http.StatusOK,
		GameStatistic: gameStatistic,
	}

	err = json.NewEncoder(w).Encode(resp)

	if err != nil {
		c.Logger.Error(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}
}

func (c *Controller) GameStatisticByTeamIDHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	if c.requestTimeOut > 0 {
		var cancel context.CancelFunc

		ctx, cancel = context.WithTimeout(ctx, time.Duration(c.requestTimeOut)*time.Second)

		defer cancel()
	}

	vars := mux.Vars(r)
	teamIdString := vars["team_id"]

	teamId, err := strconv.Atoi(teamIdString)
	if err != nil {
		c.Logger.Error(err.Error())

		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	var resp apiModels.GameStatisticByTeamIDResponse

	gameStatistic, err := c.service.GameStatisticByTeamID(ctx, uint(teamId))

	if err != nil {
		c.Logger.Error(err.Error())
		resp.Status = http.StatusInternalServerError

		err = json.NewEncoder(w).Encode(resp)

		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	resp = models.GameStatisticByTeamIDResponse{
		Status:        http.StatusOK,
		GameStatistic: gameStatistic,
	}

	err = json.NewEncoder(w).Encode(resp)

	if err != nil {
		c.Logger.Error(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}
}
