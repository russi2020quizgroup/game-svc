package controller

import (
	g "gitlab.com/Russi2020QuizGroup/game-svc/internal/services/interfaces"
	li "gitlab.com/Russi2020QuizGroup/game-svc/pkg/logger/interfaces"
	rt "gitlab.com/Russi2020QuizGroup/game-svc/pkg/router/types"
)

type Controller struct {
	GameSvcUrl     string
	service        g.GameSVCInterface
	requestTimeOut int
	Logger         li.Logger
	routes         rt.Routes
	QuizAPIPath    string
}

func New(
	gameSvcUrl string,
	service g.GameSVCInterface,
	rt int,
	logger li.Logger,
	quizAPIPath string,
) *Controller {
	controller := &Controller{
		GameSvcUrl:     gameSvcUrl,
		service:        service,
		requestTimeOut: rt,
		Logger:         logger,
		QuizAPIPath:    quizAPIPath,
	}
	controller.AddRoutes()
	return controller
}
