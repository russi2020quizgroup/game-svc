package controller

import (
	"context"
	"encoding/json"
	"github.com/gorilla/mux"
	"gitlab.com/Russi2020QuizGroup/game-svc/internal/controller/interfaces"
	"gitlab.com/Russi2020QuizGroup/game-svc/internal/models"
	apiModels "gitlab.com/Russi2020QuizGroup/game-svc/internal/models"
	"net/http"
	"strconv"
	"time"
)

var _ interfaces.TeamGameAnswerController = (*Controller)(nil)

func (c *Controller) AddTeamGameAnswerHandler(w http.ResponseWriter, r *http.Request) {
	var req apiModels.AddTeamGameAnswerRequest
	ctx := r.Context()
	if c.requestTimeOut > 0 {
		var cancel context.CancelFunc

		ctx, cancel = context.WithTimeout(ctx, time.Duration(c.requestTimeOut)*time.Second)
		defer cancel()
	}

	contentType := r.Header.Get("Content-Type")
	switch contentType {
	case "application/json":
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			c.Logger.Error(err.Error())
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
	default:
		http.Error(w, "Unsupported content type", http.StatusBadRequest)
		return
	}

	var resp apiModels.AddTeamGameAnswerResponse
	teamGameAnswer := models.TeamGameAnswer{
		Id:              req.Id,
		TeamID:          req.TeamID,
		GameStatisticId: req.GameStatisticId,
		RoundId:         req.RoundId,
		QuestionId:      req.QuestionId,
		AnswerID:        req.AnswerID,
		IsCorrect:       req.IsCorrect,
	}

	err := c.service.AddTeamGameAnswer(ctx, teamGameAnswer)

	if err != nil {
		c.Logger.Error(err.Error())
		resp.Status = http.StatusInternalServerError
		err = json.NewEncoder(w).Encode(resp)

		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	resp = models.AddTeamGameAnswerResponse{
		Status: http.StatusCreated,
	}

	err = json.NewEncoder(w).Encode(resp)

	if err != nil {
		c.Logger.Error(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (c *Controller) TeamGameAnswersByGameIdHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	if c.requestTimeOut > 0 {
		var cancel context.CancelFunc

		ctx, cancel = context.WithTimeout(ctx, time.Duration(c.requestTimeOut)*time.Second)

		defer cancel()
	}

	vars := mux.Vars(r)
	gameIdString := vars["game_id"]

	gameId, err := strconv.Atoi(gameIdString)
	if err != nil {
		c.Logger.Error(err.Error())
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	var resp apiModels.TeamGameAnswersByGameIdResponse

	teamGameAnswers, err := c.service.TeamGameAnswersByGameId(ctx, uint(gameId))

	if err != nil {
		c.Logger.Error(err.Error())
		resp.Status = http.StatusInternalServerError
		err = json.NewEncoder(w).Encode(resp)

		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	resp = models.TeamGameAnswersByGameIdResponse{
		Status:          http.StatusOK,
		TeamGameAnswers: teamGameAnswers,
	}

	err = json.NewEncoder(w).Encode(resp)

	if err != nil {
		c.Logger.Error(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (c *Controller) UpdateTeamGameAnswerHandler(w http.ResponseWriter, r *http.Request) {
	var req apiModels.UpdateTeamGameAnswerRequest
	ctx := r.Context()
	if c.requestTimeOut > 0 {
		var cancel context.CancelFunc

		ctx, cancel = context.WithTimeout(ctx, time.Duration(c.requestTimeOut)*time.Second)

		defer cancel()
	}

	contentType := r.Header.Get("Content-Type")
	switch contentType {
	case "application/json":
		err := json.NewDecoder(r.Body).Decode(&req)

		if err != nil {
			c.Logger.Error(err.Error())
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
	default:
		http.Error(w, "Unsupported content type", http.StatusBadRequest)

		return
	}

	var resp apiModels.UpdateTeamGameAnswerResponse
	teamGameAnswer := models.TeamGameAnswer{
		Id:        req.Id,
		AnswerID:  req.AnswerID,
		IsCorrect: req.IsCorrect,
	}

	err := c.service.UpdateTeamGameAnswer(ctx, teamGameAnswer)

	if err != nil {
		c.Logger.Error(err.Error())
		resp.Status = http.StatusInternalServerError

		err = json.NewEncoder(w).Encode(resp)

		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	resp = models.UpdateTeamGameAnswerResponse{
		Status: http.StatusOK,
	}

	err = json.NewEncoder(w).Encode(resp)

	if err != nil {
		c.Logger.Error(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}
}

func (c *Controller) DeleteTeamGameAnswerHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	if c.requestTimeOut > 0 {
		var cancel context.CancelFunc

		ctx, cancel = context.WithTimeout(ctx, time.Duration(c.requestTimeOut)*time.Second)

		defer cancel()
	}

	vars := mux.Vars(r)
	idString := vars["id"]
	id, err := strconv.Atoi(idString)
	if err != nil {
		c.Logger.Error(err.Error())

		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	var resp apiModels.DeleteTeamGameAnswerResponse

	err = c.service.DeleteTeamGameAnswer(ctx, uint(id))

	if err != nil {
		c.Logger.Error(err.Error())
		resp.Status = http.StatusInternalServerError

		err = json.NewEncoder(w).Encode(resp)

		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	resp = models.DeleteTeamGameAnswerResponse{
		Status: http.StatusOK,
	}

	err = json.NewEncoder(w).Encode(resp)

	if err != nil {
		c.Logger.Error(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}
}
