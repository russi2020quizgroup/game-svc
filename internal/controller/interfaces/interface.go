package interfaces

import (
	rt "gitlab.com/Russi2020QuizGroup/game-svc/pkg/router/types"
	"net/http"
)

type Controller interface {
	RoutesMaker
	RoutesProvider
}

type PlayerController interface {
	AddPlayerHandler(w http.ResponseWriter, r *http.Request)
	PlayerByEmailHandler(w http.ResponseWriter, r *http.Request)
	PlayersByUsernameHandler(w http.ResponseWriter, r *http.Request)
	UpdatePlayerUsernameHandler(w http.ResponseWriter, r *http.Request)
	UpdatePlayerEmailHandler(w http.ResponseWriter, r *http.Request)
	DeletePlayerHandler(w http.ResponseWriter, r *http.Request)
}

type TeamController interface {
	AddTeamHandler(w http.ResponseWriter, r *http.Request)
	AddTeamMemberHandler(w http.ResponseWriter, r *http.Request)
	TeamsByNameHandler(w http.ResponseWriter, r *http.Request)
	TeamsHandler(w http.ResponseWriter, r *http.Request)
	AllTeamMembersHandler(w http.ResponseWriter, r *http.Request)
	UpdateTeamNameHandler(w http.ResponseWriter, r *http.Request)
	DeleteTeamHandler(w http.ResponseWriter, r *http.Request)
}

type SingleGameAnswerController interface {
	AddSingleGameAnswerHandler(w http.ResponseWriter, r *http.Request)
	SingleGameAnswersByGameIdHandler(w http.ResponseWriter, r *http.Request)
	UpdateSingleGameAnswerHandler(w http.ResponseWriter, r *http.Request)
	DeleteSingleGameAnswerHandler(w http.ResponseWriter, r *http.Request)
}

type TeamGameAnswerController interface {
	AddTeamGameAnswerHandler(w http.ResponseWriter, r *http.Request)
	TeamGameAnswersByGameIdHandler(w http.ResponseWriter, r *http.Request)
	UpdateTeamGameAnswerHandler(w http.ResponseWriter, r *http.Request)
	DeleteTeamGameAnswerHandler(w http.ResponseWriter, r *http.Request)
}

type GameStatisticController interface {
	AddGameStatisticHandler(w http.ResponseWriter, r *http.Request)
	GameStatisticByGameIDHandler(w http.ResponseWriter, r *http.Request)
	GameStatisticByPlayerIDHandler(w http.ResponseWriter, r *http.Request)
	GameStatisticByTeamIDHandler(w http.ResponseWriter, r *http.Request)
}

type QuizAPIController interface {
	CategoriesHandler(w http.ResponseWriter, r *http.Request)
	ThemesHandler(w http.ResponseWriter, r *http.Request)
	GameThemesControllerHandler(w http.ResponseWriter, r *http.Request)
	GameByGameThemeIdHandler(w http.ResponseWriter, r *http.Request)
}

type RoutesMaker interface {
	AddRoutes()
}

type RoutesProvider interface {
	Routes() rt.Routes
}
