package interfaces

import (
	"context"
	"gitlab.com/Russi2020QuizGroup/game-svc/internal/models"
	ct "gitlab.com/Russi2020QuizGroup/game-svc/pkg/custom_types"
)

type StorageInterface interface {
	PlayerRepository
	TeamRepository
	GameStatisticRepository
	SingleGameAnswerRepository
	TeamGameAnswerRepository
}

type PlayerRepository interface {
	AddPlayerToDB(ctx context.Context, player models.Player) error
	PlayerByEmailToDB(ctx context.Context, email string) (models.Player, error)
	PlayersByUsernameToDB(ctx context.Context, email string) ([]models.Player, error)
	Players(ctx context.Context) ([]models.Player, error)
	UpdatePlayerUsername(ctx context.Context, player models.Player) error
	UpdatePlayerEmail(ctx context.Context, player models.Player) error
	DeletePlayer(ctx context.Context, id uint) error
}

type TeamRepository interface {
	AddTeamToDB(ctx context.Context, team models.Team) error
	AddTeamMember(ctx context.Context, playerId uint, teamId uint) error
	TeamsByName(ctx context.Context, name string) ([]models.Team, error)
	AllTeamMembers(ctx context.Context, teamId uint) ([]models.Player, error)
	Teams(ctx context.Context) ([]models.Team, error)
	UpdateTeamName(ctx context.Context, team models.Team) error
	DeleteTeam(ctx context.Context, id uint) error
}

type SingleGameAnswerRepository interface {
	AddSingleGameAnswerToDB(ctx context.Context, participantAnswer models.SingleGameAnswer) error
	SingleGameAnswersByGameId(ctx context.Context, gameId uint) ([]models.SingleGameAnswer, error)
	UpdateSingleGameAnswer(ctx context.Context, singleGameAnswer models.SingleGameAnswer) error
	DeleteSingleGameAnswer(ctx context.Context, id uint) error
}

type TeamGameAnswerRepository interface {
	AddTeamGameAnswerToDB(ctx context.Context, participantAnswer models.TeamGameAnswer) error
	TeamGameAnswersByGameId(ctx context.Context, gameId uint) ([]models.TeamGameAnswer, error)
	UpdateTeamGameAnswer(ctx context.Context, teamGameAnswer models.TeamGameAnswer) error
	DeleteTeamGameAnswer(ctx context.Context, id uint) error
}

type GameStatisticRepository interface {
	AddGameStatisticToDB(ctx context.Context, gameId uint, date ct.CustomTime, startTime ct.NullTime) error
	GameStatisticByGameID(ctx context.Context, gameId uint) (models.GameStatistic, error)
	GameStatisticByPlayerID(ctx context.Context, playerId uint) ([]models.GameStatistic, error)
	GameStatisticByTeamID(ctx context.Context, teamId uint) ([]models.GameStatistic, error)
}
