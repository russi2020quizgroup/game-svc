package postgresdb

import (
	"context"
	"gitlab.com/Russi2020QuizGroup/game-svc/internal/models"
)

func (s *Storage) AddTeamGameAnswerToDB(ctx context.Context, participantAnswer models.TeamGameAnswer) error {
	query := "CALL participants.add_team_game_answer($1, $2, $3, $4, $5, $6)"
	_, err := s.Db.Exec(
		ctx,
		query,
		participantAnswer.TeamID,
		participantAnswer.GameStatisticId,
		participantAnswer.RoundId,
		participantAnswer.QuestionId,
		participantAnswer.AnswerID,
		participantAnswer.IsCorrect,
	)

	if err != nil {
		return err
	}

	return nil
}

func (s *Storage) TeamGameAnswersByGameId(ctx context.Context, gameId uint) ([]models.TeamGameAnswer, error) {
	query := "SELECT * FROM participants.get_team_game_answers_by_game_id($1)"
	rows, err := s.Db.Query(ctx, query, gameId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var answers []models.TeamGameAnswer
	for rows.Next() {
		var answer models.TeamGameAnswer
		if err := rows.Scan(
			&answer.Id,
			&answer.TeamID,
			&answer.GameStatisticId,
			&answer.RoundId,
			&answer.QuestionId,
			&answer.AnswerID,
			&answer.IsCorrect,
		); err != nil {
			return nil, err
		}
		answers = append(answers, answer)
	}

	// check for any row scan errors
	if err := rows.Err(); err != nil {
		return nil, err
	}

	return answers, nil

}

func (s *Storage) UpdateTeamGameAnswer(ctx context.Context, teamGameAnswer models.TeamGameAnswer) error {
	query := "CALL participants.update_team_game_answer($1, $2, $3)"
	_, err := s.Db.Exec(
		ctx,
		query,
		teamGameAnswer.Id,
		teamGameAnswer.AnswerID,
		teamGameAnswer.IsCorrect,
	)

	if err != nil {
		return err
	}

	return nil
}

func (s *Storage) DeleteTeamGameAnswer(ctx context.Context, id uint) error {
	query := "CALL participants.delete_team_game_answer($1)"
	_, err := s.Db.Exec(
		ctx,
		query,
		id,
	)

	if err != nil {
		return err
	}

	return nil
}
