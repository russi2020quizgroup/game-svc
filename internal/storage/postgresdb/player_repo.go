package postgresdb

import (
	"context"
	"gitlab.com/Russi2020QuizGroup/game-svc/internal/models"
)

func (s *Storage) AddPlayerToDB(ctx context.Context, player models.Player) error {

	_, err := s.Db.Exec(ctx, "CALL participants.add_player($1, $2)", player.Username, player.Email)
	if err != nil {
		return err
	}

	return nil
}

func (s *Storage) PlayerByEmailToDB(ctx context.Context, email string) (models.Player, error) {
	rows, err := s.Db.Query(ctx, "SELECT * FROM participants.player_by_email($1)", email)
	if err != nil {
		return models.Player{}, err
	}
	defer rows.Close()

	var player models.Player
	if rows.Next() {
		if err := rows.Scan(&player.Id, &player.Username, &player.Email, &player.TeamId); err != nil {
			return models.Player{}, err
		}
	}

	// check for any row scan errors
	if err := rows.Err(); err != nil {
		return models.Player{}, err
	}

	return player, nil
}

func (s *Storage) PlayersByUsernameToDB(ctx context.Context, username string) ([]models.Player, error) {
	rows, err := s.Db.Query(ctx, "SELECT * FROM participants.player_by_username($1)", username)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var players []models.Player
	for rows.Next() {
		var player models.Player
		if err := rows.Scan(&player.Id, &player.Username, &player.Email, &player.TeamId); err != nil {
			return nil, err
		}
		players = append(players, player)
	}

	// check for any row scan errors
	if err := rows.Err(); err != nil {
		return nil, err
	}

	return players, nil
}

func (s *Storage) Players(ctx context.Context) ([]models.Player, error) {
	rows, err := s.Db.Query(ctx, "SELECT * FROM participants.players()")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var players []models.Player

	for rows.Next() {
		var player models.Player
		if err := rows.Scan(&player.Id, &player.Username, &player.Email, &player.TeamId); err != nil {
			return nil, err
		}
		players = append(players, player)
	}

	return players, nil
}

func (s *Storage) UpdatePlayerUsername(ctx context.Context, player models.Player) error {
	_, err := s.Db.Exec(ctx, "CALL participants.update_player_username($1, $2)", player.Id, player.Username)

	if err != nil {
		return err
	}

	return nil
}

func (s *Storage) UpdatePlayerEmail(ctx context.Context, player models.Player) error {
	_, err := s.Db.Exec(ctx, "CALL participants.update_player_email($1, $2)", player.Id, player.Email)

	if err != nil {
		return err
	}

	return nil
}

func (s *Storage) DeletePlayer(ctx context.Context, id uint) error {
	_, err := s.Db.Exec(ctx, "CALL participants.delete_player($1)", id)

	if err != nil {
		return err
	}

	return nil
}
