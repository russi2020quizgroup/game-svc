package postgresdb

import (
	"context"
	"github.com/jackc/pgx/v4/pgxpool"
	si "gitlab.com/Russi2020QuizGroup/game-svc/internal/storage/interfaces"
)

var _ si.StorageInterface = (*Storage)(nil)

type Storage struct {
	Db *pgxpool.Pool
}

// New Создает новый экземпляр структуры Storage
func New(db *pgxpool.Pool) *Storage {
	return &Storage{
		Db: db,
	}
}

// NewDbPool создает инстанс пула соединений postgres
func NewDbPool(ctx context.Context, constr string) (*pgxpool.Pool, error) {
	db, err := pgxpool.Connect(ctx, constr)
	if err != nil {
		return nil, err
	}

	return db, nil
}
