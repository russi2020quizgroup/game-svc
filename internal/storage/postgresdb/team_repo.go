package postgresdb

import (
	"context"
	"fmt"
	"gitlab.com/Russi2020QuizGroup/game-svc/internal/models"
)

func (s *Storage) AddTeamToDB(ctx context.Context, team models.Team) error {
	// create team in db
	var teamId uint
	err := s.Db.QueryRow(ctx, "SELECT participants.add_team($1)", team.Name).Scan(&teamId)

	if err != nil {
		return fmt.Errorf("error while creating team in db: %v", err)
	}

	team.Id = teamId

	// add members to team
	for _, player := range team.Players {
		if err := s.AddTeamMember(ctx, player.Id, team.Id); err != nil {
			return err
		}
	}

	return nil
}

func (s *Storage) AddTeamMember(ctx context.Context, playerId uint, teamId uint) error {
	_, err := s.Db.Exec(ctx, "CALL participants.add_team_member($1, $2)", playerId, teamId)
	if err != nil {
		return err
	}

	return nil
}

func (s *Storage) TeamsByName(ctx context.Context, name string) ([]models.Team, error) {
	rows, err := s.Db.Query(ctx, "SELECT * FROM participants.find_team_by_name($1)", name)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var teams []models.Team

	for rows.Next() {
		var team models.Team
		if err := rows.Scan(&team.Id, &team.Name); err != nil {
			return nil, err
		}
		teams = append(teams, team)
	}

	// check for any row scan errors
	if err := rows.Err(); err != nil {
		return nil, err
	}

	return teams, nil
}

func (s *Storage) AllTeamMembers(ctx context.Context, teamId uint) ([]models.Player, error) {
	rows, err := s.Db.Query(ctx, "SELECT * FROM participants.all_team_members($1)", teamId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var players []models.Player

	for rows.Next() {
		var player models.Player
		if err := rows.Scan(&player.Id, &player.Username, &player.Email, &player.TeamId); err != nil {
			return nil, err
		}
		players = append(players, player)
	}

	return players, nil
}

func (s *Storage) Teams(ctx context.Context) ([]models.Team, error) {
	rows, err := s.Db.Query(ctx, "SELECT * FROM participants.teams()")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	teamMap := make(map[uint]*models.Team)

	for rows.Next() {
		var teamID uint
		var teamName string
		var player models.Player

		if err := rows.Scan(&teamID, &teamName, &player.Id, &player.Username, &player.Email); err != nil {
			return nil, err
		}

		team, exists := teamMap[teamID]
		if !exists {
			team = &models.Team{
				Id:      teamID,
				Name:    teamName,
				Players: []models.Player{},
			}
			teamMap[teamID] = team
		}

		team.Players = append(team.Players, player)
	}

	teams := make([]models.Team, 0, len(teamMap))
	for _, team := range teamMap {
		teams = append(teams, *team)
	}

	return teams, nil
}

func (s *Storage) UpdateTeamName(ctx context.Context, team models.Team) error {
	_, err := s.Db.Exec(ctx, "CALL participants.update_team_name($1, $2)", team.Id, team.Name)

	if err != nil {
		return err
	}

	return nil
}

func (s *Storage) DeleteTeam(ctx context.Context, id uint) error {
	_, err := s.Db.Exec(ctx, "CALL participants.delete_team($1)", id)

	if err != nil {
		return err
	}

	return nil
}
