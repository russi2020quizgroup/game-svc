package postgresdb

import (
	"context"
	"gitlab.com/Russi2020QuizGroup/game-svc/internal/models"
	ct "gitlab.com/Russi2020QuizGroup/game-svc/pkg/custom_types"
)

func (s *Storage) AddGameStatisticToDB(ctx context.Context, gameId uint, date ct.CustomTime, startTime ct.NullTime) error {
	_, err := s.Db.Exec(ctx, "CALL game.add_game_statistic($1, $2, $3)", gameId, date.Time, startTime.Time)

	if err != nil {
		return err
	}

	return nil
}

func (s *Storage) GameStatisticByGameID(ctx context.Context, gameId uint) (models.GameStatistic, error) {
	rows, err := s.Db.Query(ctx, "SELECT * FROM game.get_game_statistic($1)", gameId)
	if err != nil {
		return models.GameStatistic{}, err
	}
	defer rows.Close()

	var prevId uint
	var gameStatistic models.GameStatistic
	for rows.Next() {
		var result models.PlayerResults
		var id uint
		if err := rows.Scan(&id, &gameStatistic.GameId, &gameStatistic.Date, &gameStatistic.StartTime, &gameStatistic.EndTime,
			&result.TotalScore, &result.TotalQuestions, &result.PercentCorrectAnswers, &result.Player.Id,
			&result.Player.Username, &result.Player.Email); err != nil {
			return models.GameStatistic{}, err
		}

		if id != prevId {
			gameStatistic.Id = id
			gameStatistic.Results = append(gameStatistic.Results, models.GameResults{})
			prevId = id
		}
		gameStatistic.Results[len(gameStatistic.Results)-1].PlayerResults =
			append(gameStatistic.Results[len(gameStatistic.Results)-1].PlayerResults, result)
	}
	// Check for errors from rows.Err
	if err := rows.Err(); err != nil {
		return models.GameStatistic{}, err
	}

	return gameStatistic, nil
}

func (s *Storage) GameStatisticByPlayerID(ctx context.Context, playerId uint) ([]models.GameStatistic, error) {
	rows, err := s.Db.Query(ctx, "SELECT * FROM game.get_game_statistic_by_player_id($1)", playerId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var prevId uint
	var gameStatistics []models.GameStatistic
	for rows.Next() {
		var result models.PlayerResults
		var id uint
		var gameStatistic models.GameStatistic
		if err := rows.Scan(&id, &gameStatistic.GameId, &gameStatistic.Date, &gameStatistic.StartTime, &gameStatistic.EndTime,
			&result.TotalScore, &result.TotalQuestions, &result.PercentCorrectAnswers, &result.Player.Id,
			&result.Player.Username, &result.Player.Email); err != nil {
			return nil, err
		}

		if id != prevId {
			gameStatistic.Id = id
			gameStatistic.Results = append(gameStatistic.Results, models.GameResults{})
			prevId = id
		}
		gameStatistic.Results[len(gameStatistic.Results)-1].PlayerResults =
			append(gameStatistic.Results[len(gameStatistic.Results)-1].PlayerResults, result)
		gameStatistics = append(gameStatistics, gameStatistic)
	}
	// Check for errors from rows.Err
	if err := rows.Err(); err != nil {
		return nil, err
	}

	return gameStatistics, nil
}

func (s *Storage) GameStatisticByTeamID(ctx context.Context, teamId uint) ([]models.GameStatistic, error) {
	rows, err := s.Db.Query(ctx, "SELECT * FROM game.get_game_statistic_by_team_id($1)", teamId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var prevId uint
	var gameStatistics []models.GameStatistic
	for rows.Next() {
		var result models.TeamResults
		var id uint
		var gameStatistic models.GameStatistic
		if err := rows.Scan(&id, &gameStatistic.GameId, &gameStatistic.Date, &gameStatistic.StartTime, &gameStatistic.EndTime,
			&result.TotalScore, &result.TotalQuestions, &result.PercentCorrectAnswers, &result.Team.Id,
			&result.Team.Name); err != nil {
			return nil, err
		}

		if id != prevId {
			gameStatistic.Id = id
			gameStatistic.Results = append(gameStatistic.Results, models.GameResults{})
			prevId = id
		}
		gameStatistic.Results[len(gameStatistic.Results)-1].TeamResults =
			append(gameStatistic.Results[len(gameStatistic.Results)-1].TeamResults, result)
		gameStatistics = append(gameStatistics, gameStatistic)
	}
	// Check for errors from rows.Err
	if err := rows.Err(); err != nil {
		return nil, err
	}

	return gameStatistics, nil
}
