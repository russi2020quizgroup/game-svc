package postgresdb

import (
	"context"
	"gitlab.com/Russi2020QuizGroup/game-svc/internal/models"
)

func (s *Storage) AddSingleGameAnswerToDB(ctx context.Context, participantAnswer models.SingleGameAnswer) error {
	query := "CALL participants.add_single_game_answer($1, $2, $3, $4, $5, $6)"
	_, err := s.Db.Exec(
		ctx,
		query,
		participantAnswer.PlayerID,
		participantAnswer.GameStatisticId,
		participantAnswer.RoundId,
		participantAnswer.QuestionId,
		participantAnswer.AnswerID,
		participantAnswer.IsCorrect,
	)

	if err != nil {
		return err
	}

	return nil
}

func (s *Storage) SingleGameAnswersByGameId(ctx context.Context, gameId uint) ([]models.SingleGameAnswer, error) {
	query := "SELECT * FROM participants.single_game_answers_by_game_id($1)"
	rows, err := s.Db.Query(ctx, query, gameId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var answers []models.SingleGameAnswer
	for rows.Next() {
		var answer models.SingleGameAnswer
		if err := rows.Scan(
			&answer.Id,
			&answer.PlayerID,
			&answer.GameStatisticId,
			&answer.RoundId,
			&answer.QuestionId,
			&answer.AnswerID,
			&answer.IsCorrect,
		); err != nil {
			return nil, err
		}
		answers = append(answers, answer)
	}

	// check for any row scan errors
	if err := rows.Err(); err != nil {
		return nil, err
	}

	return answers, nil
}

func (s *Storage) UpdateSingleGameAnswer(ctx context.Context, singleGameAnswer models.SingleGameAnswer) error {
	query := "CALL participants.update_single_game_answer($1, $2, $3)"
	_, err := s.Db.Exec(
		ctx,
		query,
		singleGameAnswer.Id,
		singleGameAnswer.AnswerID,
		singleGameAnswer.IsCorrect,
	)

	if err != nil {
		return err
	}

	return nil
}

func (s *Storage) DeleteSingleGameAnswer(ctx context.Context, id uint) error {
	query := "CALL participants.delete_single_game_answer($1)"
	_, err := s.Db.Exec(
		ctx,
		query,
		id,
	)

	if err != nil {
		return err
	}

	return nil
}
