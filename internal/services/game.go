package services

import (
	"context"
	"gitlab.com/Russi2020QuizGroup/game-svc/internal/models"
	sci "gitlab.com/Russi2020QuizGroup/game-svc/internal/services/interfaces"
	si "gitlab.com/Russi2020QuizGroup/game-svc/internal/storage/interfaces"
	ct "gitlab.com/Russi2020QuizGroup/game-svc/pkg/custom_types"
	li "gitlab.com/Russi2020QuizGroup/game-svc/pkg/logger/interfaces"
	rt "gitlab.com/Russi2020QuizGroup/game-svc/pkg/router/types"
)

var _ sci.GameSVCInterface = (*GameSVC)(nil)

type GameSVC struct {
	BaseUrl        string
	db             si.StorageInterface
	RequestTimeOut int
	Logger         li.Logger
	routes         rt.Routes
}

// NewGameSVC создает новый инстанс SchedulerSVC
func NewGameSVC(db si.StorageInterface, rt int, logger li.Logger, baseUrl string) *GameSVC {
	game := &GameSVC{
		db:             db,
		RequestTimeOut: rt,
		Logger:         logger,
		BaseUrl:        baseUrl,
	}
	return game
}

func (g *GameSVC) AddPlayer(ctx context.Context, player models.Player) error {
	err := g.db.AddPlayerToDB(ctx, player)
	if err != nil {
		g.Logger.Error(err.Error())
		return err
	}

	return nil
}

func (g *GameSVC) PlayerByEmail(ctx context.Context, email string) (models.Player, error) {
	player, err := g.db.PlayerByEmailToDB(ctx, email)
	if err != nil {
		g.Logger.Error(err.Error())
		return models.Player{}, err
	}

	return player, nil
}

func (g *GameSVC) PlayersByUsername(ctx context.Context, email string) ([]models.Player, error) {
	players, err := g.db.PlayersByUsernameToDB(ctx, email)
	if err != nil {
		g.Logger.Error(err.Error())
		return nil, err
	}

	return players, nil
}

func (g *GameSVC) Players(ctx context.Context) ([]models.Player, error) {
	players, err := g.db.Players(ctx)
	if err != nil {
		g.Logger.Error(err.Error())
		return nil, err
	}

	return players, nil
}

func (g *GameSVC) UpdatePlayerUsername(ctx context.Context, player models.Player) error {
	err := g.db.UpdatePlayerUsername(ctx, player)
	if err != nil {
		g.Logger.Error(err.Error())
		return err
	}

	return nil
}

func (g *GameSVC) UpdatePlayerEmail(ctx context.Context, player models.Player) error {
	err := g.db.UpdatePlayerEmail(ctx, player)
	if err != nil {
		g.Logger.Error(err.Error())
		return err
	}

	return nil
}

func (g *GameSVC) DeletePlayer(ctx context.Context, id uint) error {
	err := g.db.DeletePlayer(ctx, id)
	if err != nil {
		g.Logger.Error(err.Error())
		return err
	}

	return nil
}

func (g *GameSVC) AddTeam(ctx context.Context, team models.Team) error {
	err := g.db.AddTeamToDB(ctx, team)
	if err != nil {
		g.Logger.Error(err.Error())
		return err
	}

	return nil
}

func (g *GameSVC) AddTeamMember(ctx context.Context, playerId, teamId uint) error {
	err := g.db.AddTeamMember(ctx, playerId, teamId)
	if err != nil {
		g.Logger.Error(err.Error())
		return err
	}

	return nil
}

func (g *GameSVC) TeamsByName(ctx context.Context, name string) ([]models.Team, error) {
	teams, err := g.db.TeamsByName(ctx, name)
	if err != nil {
		g.Logger.Error(err.Error())
		return nil, err
	}

	return teams, nil
}

func (g *GameSVC) AllTeamMembers(ctx context.Context, teamId uint) ([]models.Player, error) {
	players, err := g.db.AllTeamMembers(ctx, teamId)
	if err != nil {
		g.Logger.Error(err.Error())
		return nil, err
	}

	return players, nil
}

func (g *GameSVC) Teams(ctx context.Context) ([]models.Team, error) {
	teams, err := g.db.Teams(ctx)
	if err != nil {
		g.Logger.Error(err.Error())
		return nil, err
	}

	return teams, nil
}

func (g *GameSVC) UpdateTeamName(ctx context.Context, team models.Team) error {
	err := g.db.UpdateTeamName(ctx, team)
	if err != nil {
		g.Logger.Error(err.Error())
		return err
	}

	return nil
}

func (g *GameSVC) DeleteTeam(ctx context.Context, id uint) error {
	err := g.db.DeleteTeam(ctx, id)

	if err != nil {
		g.Logger.Error(err.Error())
		return err
	}

	return nil
}

func (g *GameSVC) AddSingleGameAnswer(ctx context.Context, participantAnswer models.SingleGameAnswer) error {
	err := g.db.AddSingleGameAnswerToDB(ctx, participantAnswer)

	if err != nil {
		return err
	}

	return nil
}

func (g *GameSVC) SingleGameAnswersByGameId(ctx context.Context, gameId uint) ([]models.SingleGameAnswer, error) {
	singleGameAnswers, err := g.db.SingleGameAnswersByGameId(ctx, gameId)

	if err != nil {
		return nil, err
	}

	return singleGameAnswers, nil
}

func (g *GameSVC) AddGameStatistic(ctx context.Context, gameId uint, date ct.CustomTime, startTime ct.NullTime) error {
	err := g.db.AddGameStatisticToDB(ctx, gameId, date, startTime)

	if err != nil {
		return err
	}

	return nil
}

func (g *GameSVC) GameStatisticByGameID(ctx context.Context, gameId uint) (models.GameStatistic, error) {
	gameStatistic, err := g.db.GameStatisticByGameID(ctx, gameId)

	if err != nil {
		return models.GameStatistic{}, err
	}

	return gameStatistic, nil
}

func (g *GameSVC) UpdateSingleGameAnswer(ctx context.Context, singleGameAnswer models.SingleGameAnswer) error {
	err := g.db.UpdateSingleGameAnswer(ctx, singleGameAnswer)

	if err != nil {
		return err
	}

	return nil
}

func (g *GameSVC) DeleteSingleGameAnswer(ctx context.Context, id uint) error {
	err := g.db.DeleteSingleGameAnswer(ctx, id)

	if err != nil {
		return err
	}

	return nil
}

func (g *GameSVC) AddTeamGameAnswer(ctx context.Context, teamGameAnswer models.TeamGameAnswer) error {
	err := g.db.AddTeamGameAnswerToDB(ctx, teamGameAnswer)

	if err != nil {
		return err
	}

	return nil
}

func (g *GameSVC) TeamGameAnswersByGameId(ctx context.Context, gameId uint) ([]models.TeamGameAnswer, error) {
	teamGameAnswers, err := g.db.TeamGameAnswersByGameId(ctx, gameId)

	if err != nil {
		return nil, err
	}

	return teamGameAnswers, nil
}

func (g *GameSVC) UpdateTeamGameAnswer(ctx context.Context, teamGameAnswer models.TeamGameAnswer) error {
	err := g.db.UpdateTeamGameAnswer(ctx, teamGameAnswer)

	if err != nil {
		return err
	}

	return nil
}

func (g *GameSVC) DeleteTeamGameAnswer(ctx context.Context, id uint) error {
	err := g.db.DeleteTeamGameAnswer(ctx, id)

	if err != nil {
		return err
	}

	return nil
}

func (g *GameSVC) GameStatisticByPlayerID(ctx context.Context, playerId uint) ([]models.GameStatistic, error) {
	gameStatistic, err := g.db.GameStatisticByPlayerID(ctx, playerId)

	if err != nil {
		return nil, err
	}

	return gameStatistic, nil
}

func (g *GameSVC) GameStatisticByTeamID(ctx context.Context, teamId uint) ([]models.GameStatistic, error) {
	gameStatistic, err := g.db.GameStatisticByTeamID(ctx, teamId)

	if err != nil {
		return nil, err
	}

	return gameStatistic, nil
}
