package bootstrap

import "gitlab.com/Russi2020QuizGroup/game-svc/pkg/config"

func ProvideConfig() (*config.Config, error) {
	return config.LoadConfig()
}
