package bootstrap

import (
	"gitlab.com/Russi2020QuizGroup/game-svc/pkg/config"
	"gitlab.com/Russi2020QuizGroup/game-svc/pkg/logger"
	li "gitlab.com/Russi2020QuizGroup/game-svc/pkg/logger/interfaces"
)

func ProvideLogger(config *config.Config) (li.Logger, error) {
	loggerInstance, err := logger.NewLogger(config.DebugON)
	if err != nil {
		return nil, err
	}
	return loggerInstance, nil
}
