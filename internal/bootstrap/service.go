package bootstrap

import (
	quiz "gitlab.com/Russi2020QuizGroup/game-svc/internal/services"
	gameInt "gitlab.com/Russi2020QuizGroup/game-svc/internal/services/interfaces"
	si "gitlab.com/Russi2020QuizGroup/game-svc/internal/storage/interfaces"
	"gitlab.com/Russi2020QuizGroup/game-svc/pkg/config"
	li "gitlab.com/Russi2020QuizGroup/game-svc/pkg/logger/interfaces"
)

func ProvideGameService(storage si.StorageInterface, config *config.Config, logger li.Logger) gameInt.GameSVCInterface {
	return quiz.NewGameSVC(storage, config.RequestTimeout, logger, config.BaseGameSvcUrl)
}
