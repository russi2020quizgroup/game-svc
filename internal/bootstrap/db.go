package bootstrap

import (
	"context"
	si "gitlab.com/Russi2020QuizGroup/game-svc/internal/storage/interfaces"
	"gitlab.com/Russi2020QuizGroup/game-svc/internal/storage/postgresdb"
	"gitlab.com/Russi2020QuizGroup/game-svc/pkg/config"
	li "gitlab.com/Russi2020QuizGroup/game-svc/pkg/logger/interfaces"
	"os"
	"time"
)

func ProvideDB(config *config.Config, logger li.Logger) (si.StorageInterface, error) {
	curDir, err := os.Getwd()
	if err != nil {
		logger.Error(err.Error())
		return nil, err
	}

	var connString string
	if curDir == config.DockerPath {
		connString = config.DBUrlDocker
	} else {
		// Для старта локально без контейнера
		connString = config.DBUrl
	}

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	dbPool, err := postgresdb.NewDbPool(ctx, connString)

	if err != nil {
		logger.Error(err.Error())
		return nil, err
	}

	return postgresdb.New(dbPool), nil
}
