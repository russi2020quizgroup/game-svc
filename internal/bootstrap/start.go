package bootstrap

import (
	"gitlab.com/Russi2020QuizGroup/game-svc/internal/api"
	"gitlab.com/Russi2020QuizGroup/game-svc/pkg/config"
	li "gitlab.com/Russi2020QuizGroup/game-svc/pkg/logger/interfaces"
	"net/http"
)

func StartApp(apiInstance *api.API, config *config.Config, logger li.Logger) {
	logger.Infof("Start app on port %s", config.Port)

	err := http.ListenAndServe(":"+config.Port, apiInstance.Router())
	if err != nil {
		logger.Error(err.Error())
	}
}
