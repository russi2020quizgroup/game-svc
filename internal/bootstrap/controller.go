package bootstrap

import (
	"gitlab.com/Russi2020QuizGroup/game-svc/internal/controller"
	ci "gitlab.com/Russi2020QuizGroup/game-svc/internal/controller/interfaces"
	g "gitlab.com/Russi2020QuizGroup/game-svc/internal/services/interfaces"
	"gitlab.com/Russi2020QuizGroup/game-svc/pkg/config"
	li "gitlab.com/Russi2020QuizGroup/game-svc/pkg/logger/interfaces"
)

func provideController(
	svc g.GameSVCInterface,
	config *config.Config,
	logger li.Logger,
) ci.Controller {
	return controller.New(config.BaseGameSvcUrl, svc, config.RequestTimeout, logger, config.QuizAPIUrl)
}
