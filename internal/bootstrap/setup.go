package bootstrap

import (
	"go.uber.org/fx"
)

func Setup() *fx.App {
	return fx.New(
		fx.Provide(
			ProvideConfig,
			ProvideLogger,
			ProvideDB,
			provideRouter,
			ProvideGameService,
			provideController,
			ProvideAPI,
		),
		fx.Invoke(StartApp),
	)
}
