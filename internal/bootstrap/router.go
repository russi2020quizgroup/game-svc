package bootstrap

import (
	"gitlab.com/Russi2020QuizGroup/game-svc/pkg/router"
	sr "gitlab.com/Russi2020QuizGroup/game-svc/pkg/router/interfaces"
)

func provideRouter() sr.RouterInterface {
	return router.New()
}
