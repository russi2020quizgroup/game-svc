package bootstrap

import (
	"gitlab.com/Russi2020QuizGroup/game-svc/internal/api"
	ci "gitlab.com/Russi2020QuizGroup/game-svc/internal/controller/interfaces"
	li "gitlab.com/Russi2020QuizGroup/game-svc/pkg/logger/interfaces"
	ri "gitlab.com/Russi2020QuizGroup/game-svc/pkg/router/interfaces"
)

func ProvideAPI(controller ci.Controller, logger li.Logger, router ri.RouterInterface) *api.API {
	return api.New(controller, logger, router)
}
