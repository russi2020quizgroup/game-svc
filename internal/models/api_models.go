package models

import (
	ct "gitlab.com/Russi2020QuizGroup/game-svc/pkg/custom_types"
)

// swagger:model
type CreatePlayerRequest struct {
	PlayerInstance Player `json:"player"`
}

// swagger:model
type CreatePlayerResponse struct {
	// example: 201
	Status int `json:"status"`
}

// swagger:model
type PlayerByEmailRequest struct {
	// example: test@test
	Email string `json:"email"`
}

// swagger:model
type PlayerByEmailResponse struct {
	// example: 200
	Status int    `json:"status"`
	Player Player `json:"player"`
}

// swagger:model
type PlayerByUsernameResponse struct {
	// example: 200
	Status  int      `json:"status"`
	Players []Player `json:"player"`
}

// swagger:model
type PlayersResponse struct {
	// example: 200
	Status  int      `json:"status"`
	Players []Player `json:"players"`
}

// swagger:model
type UpdatePlayerUsernameRequest struct {
	// example: 1
	Id uint `json:"id"`
	// example: Test
	Username string `json:"username"`
}

// swagger:model
type UpdatePlayerUsernameResponse struct {
	// example: 200
	Status int `json:"status"`
}

// swagger:model
type UpdatePlayerEmailRequest struct {
	// example: 1
	Id uint `json:"id"`
	// example: test@test
	Email string `json:"email"`
}

// swagger:model
type UpdatePlayerEmailResponse struct {
	// example: 200
	Status int `json:"status"`
}

// swagger:model
type DeletePlayerRequest struct {
	// example: 1
	Id uint `json:"id"`
}

// swagger:model
type DeletePlayerResponse struct {
	// example: 200
	Status int `json:"status"`
}

// swagger:model
type CreateTeamRequest struct {
	// example: Test team
	Name    string   `json:"name"`
	Players []Player `json:"players,omitempty"`
}

// swagger:model
type CreateTeamResponse struct {
	// example: 201
	Status int `json:"status"`
}

// swagger:model
type AddTeamMemberRequest struct {
	// example: 1
	TeamId uint `json:"team_id"`
	// example: 1
	PlayerId uint `json:"player_id"`
}

// swagger:model
type AddTeamMemberResponse struct {
	// example: 201
	Status int `json:"status"`
}

// swagger:model
type TeamByNameResponse struct {
	// example: 200
	Status int    `json:"status"`
	Team   []Team `json:"team"`
}

// swagger:model
type AllTeamMembersResponse struct {
	// example: 200
	Status  int      `json:"status"`
	Players []Player `json:"players"`
}

// swagger:model
type TeamsResponse struct {
	// example: 200
	Status int    `json:"status"`
	Teams  []Team `json:"teams"`
}

type UpdateTeamNameResponse struct {
	// example: 200
	Status int `json:"status"`
}

type DeleteTeamResponse struct {
	// example: 200
	Status int `json:"status"`
}

type AddSingleGameAnswerRequest struct {
	// example: 1
	PlayerID ct.NullInt64 `json:"player_id,omitempty"`
	// example: 1
	GameStatisticId uint `json:"game_statistic_id"`
	// example: 1
	RoundId uint `json:"round_id"`
	// example: 1
	QuestionId uint `json:"question_id"`
	// example: 1
	AnswerID uint `json:"answer_id"`
	// example: true
	IsCorrect bool `json:"is_correct"`
}

type AddSingleGameAnswerResponse struct {
	// example: 201
	Status int `json:"status"`
}

type SingleGameAnswersByGameIdResponse struct {
	// example: 200
	Status            int                `json:"status"`
	SingleGameAnswers []SingleGameAnswer `json:"single_game_answer"`
}

type UpdateSingleGameAnswerRequest struct {
	// example: 1
	Id uint `json:"id"`
	// example: 1
	AnswerID uint `json:"answer_id"`
	// example: true
	IsCorrect bool `json:"is_correct"`
}

type UpdateSingleGameAnswerResponse struct {
	// example: 200
	Status int `json:"status"`
}

type DeleteSingleGameAnswerResponse struct {
	// example: 200
	Status int `json:"status"`
}

type AddTeamGameAnswerRequest struct {
	// example: 1
	Id uint `json:"id"`
	// example: 1
	TeamID ct.NullInt64 `json:"team_id,omitempty"`
	// example: 1
	GameStatisticId uint `json:"game_statistic_id"`
	// example: 1
	RoundId uint `json:"round_id"`
	// example: 1
	QuestionId uint `json:"question_id"`
	// example: 1
	AnswerID uint `json:"answer_id"`
	// example: true
	IsCorrect bool `json:"is_correct"`
}

type AddTeamGameAnswerResponse struct {
	// example: 201
	Status int `json:"status"`
}

type TeamGameAnswersByGameIdResponse struct {
	// example: 200
	Status          int              `json:"status"`
	TeamGameAnswers []TeamGameAnswer `json:"team_game_answers"`
}

type UpdateTeamGameAnswerRequest struct {
	// example: 1
	Id uint `json:"id"`
	// example: 1
	AnswerID uint `json:"answer_id"`
	// example: true
	IsCorrect bool `json:"is_correct"`
}

type UpdateTeamGameAnswerResponse struct {
	// example: 200
	Status int `json:"status"`
}

type DeleteTeamGameAnswerResponse struct {
	// example: 200
	Status int `json:"status"`
}

type AddGameStatisticRequest struct {
	GameId    uint          `json:"game_id"`
	Date      ct.CustomTime `json:"date"`
	StartTime ct.NullTime   `json:"start_time"`
}

type AddGameStatisticResponse struct {
	// example: 201
	Status int `json:"status"`
}

type GameStatisticByGameIDResponse struct {
	// example: 200
	Status        int           `json:"status"`
	GameStatistic GameStatistic `json:"game_statistic"`
}

type GameStatisticByPlayerIDResponse struct {
	// example: 200
	Status        int             `json:"status"`
	GameStatistic []GameStatistic `json:"games_statistic"`
}

type GameStatisticByTeamIDResponse struct {
	// example: 200
	Status        int             `json:"status"`
	GameStatistic []GameStatistic `json:"games_statistic"`
}

// QuizAPI api models

// swagger:model
type GamesResponse struct {
	// example: 200
	Status uint   `json:"status"`
	Games  []Game `json:"games"`
}

// swagger:model
type GameByGameThemeIdResponse struct {
	// example: 200
	Status uint `json:"status"`
	Game   Game `json:"game"`
}

// swagger:model
type GameByGameIdResponse struct {
	// example: 200
	Status uint `json:"status"`
	Game   Game `json:"game"`
}

// swagger:model
type RoundsByGameIdResponse struct {
	// example: 200
	Status uint    `json:"status"`
	Rounds []Round `json:"rounds"`
}

// swagger:model
type GameThemesByThemeIdResponse struct {
	// example: 200
	Status     uint         `json:"status"`
	GameThemes []*GameTheme `json:"game_themes"`
}

// swagger:model
type RoundTypeResponse struct {
	// example: 200
	Status     uint         `json:"status"`
	RoundTypes []*RoundType `json:"round_type"`
}

// swagger:model
type ThemesResponse struct {
	// example: 200
	Status uint     `json:"status"`
	Themes []*Theme `json:"themes"`
}

// swagger:model
type CategoriesResponse struct {
	// example: 200
	Status     uint        `json:"status"`
	Categories []*Category `json:"category"`
}
