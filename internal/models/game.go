package models

import (
	"database/sql"
	ct "gitlab.com/Russi2020QuizGroup/game-svc/pkg/custom_types"
	"time"
)

// swagger:model
type Player struct {
	// example: 1
	Id uint `json:"id"`
	// example: Игрок 1
	Username string `json:"username"`
	// example: email1@mail
	Email string `json:"email"`
	// example: 1
	TeamId sql.NullInt64 `json:"team_id"`
}

// swagger:model
type Team struct {
	// example: 1
	Id uint `json:"id"`
	// example: Команда 1
	Name    string   `json:"name"`
	Players []Player `json:"players"`
}

// swagger:model
type GameStatistic struct {
	// example: 1
	Id uint `json:"id"`
	// example: 1
	GameId uint `json:"game_id"`
	// example: 2020-01-01
	Date time.Time `json:"date"`
	// example: 2022-01-01 10:00:00
	StartTime ct.NullTime `json:"start_time"`
	// example: 2022-01-01 12:00:00
	EndTime ct.NullTime   `json:"end_time"`
	Results []GameResults `json:"results"`
}

// swagger:model
type GameResults struct {
	TeamResults   []TeamResults   `json:"team_results,omitempty"`
	PlayerResults []PlayerResults `json:"player_results,omitempty"`
}

// swagger:model
type TeamResults struct {
	Team Team `json:"team"`
	// example: 1
	TotalScore uint `json:"total_score"`
	// example: 1
	PercentCorrectAnswers uint `json:"percent_correct_answers"`
	// example: 1
	TotalQuestions uint `json:"total_questions"`
}

// swagger:model
type PlayerResults struct {
	Player Player `json:"player"`
	// example: 1
	TotalScore uint `json:"total_score"`
	// example: 1
	PercentCorrectAnswers uint `json:"percent_correct_answers"`
	// example: 1
	TotalQuestions uint `json:"total_questions"`
}

// swagger:model
type SingleGameAnswer struct {
	// example: 1
	Id uint `json:"id"`
	// example: 1
	PlayerID ct.NullInt64 `json:"player_id,omitempty"`
	// example: 1
	GameStatisticId uint `json:"game_statistic_id"`
	// example: 1
	RoundId uint `json:"round_id"`
	// example: 1
	QuestionId uint `json:"question_id"`
	// example: 1
	AnswerID uint `json:"answer_id"`
	// example: true
	IsCorrect bool `json:"is_correct"`
}

// swagger:model
type TeamGameAnswer struct {
	// example: 1
	Id uint `json:"id"`
	// example: 1
	TeamID ct.NullInt64 `json:"team_id,omitempty"`
	// example: 1
	GameStatisticId uint `json:"game_statistic_id"`
	// example: 1
	RoundId uint `json:"round_id"`
	// example: 1
	QuestionId uint `json:"question_id"`
	// example: 1
	AnswerID uint `json:"answer_id"`
	// example: true
	IsCorrect bool `json:"is_correct"`
}
